<?php
    $CI =& get_instance();
    if ($this->uri->segment(3) == 0)
    {
        $pelicula[0]["id"] = "";
        $pelicula[0]["titulo"] = "";
        $pelicula[0]["resumen"] = "";
        $pelicula[0]["anio"] = "";
        $pelicula[0]["pais"] = "";
        $pelicula[0]["protagonistas"] = "";
    }
    else
    {
        $CI->db->where('id', $this->uri->segment(3));
        $pelicula = $CI->db->get('peliculas')->result_array();
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Películas</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <style>
        tr {
            width: 100%;
            display: inline-table;
            table-layout: fixed;
        }

        table {
            height: 300px;
            display: -moz-groupbox;
        }

        tbody {
            overflow-y: scroll;
            height: 295px;
            width: 94%;
            position: absolute;
        }

        html {
            min-heigth: 100%;
            position: relative;
        }

        h1 {
            font-size: 50px;
            color: #616161;
        }

        body {
            margin: 0;
            margin-bottom: 120px;
            background-color: #e0e0e0;
        }

        .panel-default {
            background-color: #e0e0e0;
            border: 1px solid #bdbdbd;
        }

        .footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 100px;
            line-height: 100px;
            background-color: #424242;
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>Gestor de Películas</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Agregar Películas</div>
                    <div class="panel-body">
                        <form action="<?= base_url('peliculasController/guardar')?>" method="post">
                            <div class="col-md-12 form-group input-group">
                                <input type="hidden" name="idPelicula" id="" class="form-control" value="<?= $pelicula[0]["id"] ?>">
                            </div>
                            <div class="col-md-12 form-group input-group">
                                <label for="" class="input-group-addon">Título</label>
                                <input type="text" name="titulo" required class="form-control" value="<?= $pelicula[0]["titulo"] ?>">
                            </div>
                            <div class="col-md-12 form-group input-group">
                                <label for="" class="input-group-addon">Resumen</label>
                                <input type="text" name="resumen" required class="form-control" value="<?= $pelicula[0]["resumen"] ?>">
                            </div>
                            <div class="col-md-12 form-group input-group">
                                <label for="" class="input-group-addon">Año</label>
                                <input type="text" name="anio" required class="form-control" value="<?= $pelicula[0]["anio"] ?>">
                            </div>
                            <div class="col-md-12 form-group input-group">
                                <label for="" class="input-group-addon">País</label>
                                <input type="text" name="pais" required class="form-control" value="<?= $pelicula[0]["pais"] ?>">
                            </div>
                            <div class="col-md-12 form-group input-group">
                                <label for="" class="input-group-addon">Protagonistas</label>
                                <textarea name="protagonistas" required class="form-control">
                                    <?= $pelicula[0]["protagonistas"] ?>
                                </textarea>
                            </div>
                            <div class="col-md-12 text-center">
                                <a href="<?= base_url("peliculasController/guardar/0"); ?>" class="btn btn-primary">Nueva Película</a>
                                <button type="submit" class="btn btn-success">Guardar Película</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Películas Agregadas</div>
                    <div class="panel-body">
                        <table class="table table-hover table-striped">
                            <thead>
                                <th>Id</th>
                                <th>Título</th>
                                <th>Año</th>
                                <th>País</th>
                                <th>Protagonistas</th>
                                <th></th>
                            </thead>
                            <tbody>
                                <?php
                                    $peliculas = $CI->db->get('peliculas')->result_array();

                                    foreach($peliculas as $pelicula)
                                    {
                                        $rutaEditar = base_url("peliculasController/guardar/{$pelicula['id']}");
                                        $rutaBorrar = base_url("peliculasController/borrar?borrar={$pelicula['id']}");
                                        echo "<tr>
                                                <td>{$pelicula['id']}</td>
                                                <td>{$pelicula['titulo']}</td>
                                                <td>{$pelicula['anio']}</td>
                                                <td>{$pelicula['pais']}</td>
                                                <td>{$pelicula['protagonistas']}</td>

                                                <td>
                                                    <a href='{$rutaEditar}' class='btn btn-info glyphicon glyphicon-pencil'></a>
                                                    <a href='{$rutaBorrar}' onclick='return confirm(\"Seguro que desea eliminar esta película?\")' class='btn btn-danger glyphicon glyphicon-remove'></a>
                                                </td>
                                              </tr>";
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container text-center">
            <span class="text-muted">Derechos Reservados &copy; 2019 | Ing. Gonzalo Peñaherrera E.</span>
        </div>
    </footer>
</body>
</html>