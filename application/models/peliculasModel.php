<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PeliculasModel extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    function guardar($post)
    {
        $datosPelicula = array();
        $datosPelicula["id"] = $post["idPelicula"];
        $datosPelicula["titulo"] = $post["titulo"];
        $datosPelicula["resumen"] = $post["resumen"];
        $datosPelicula["anio"] = $post["anio"];
        $datosPelicula["pais"] = $post["pais"];
        $datosPelicula["protagonistas"] = $post["protagonistas"];
        
        if ($datosPelicula["id"] > 0)
        {
            $this->db->where('id', $datosPelicula['id']);
            $this->db->update('peliculas', $datosPelicula);
            $ruta = base_url('peliculasController');
            echo "<script>
                alert('Película modificada satisfactoriamente.');
                window.location = '{$ruta}';
            </script>";
        }
        else
        {
            $this->db->insert('peliculas', $datosPelicula);
            $ruta = base_url('peliculasController');
            echo "<script>
                alert('Película guardada satisfactoriamente.');
                window.location = '{$ruta}';
            </script>";
        }
    }

    function borrar($get)
    {
        $this->db->where('id', $get['borrar']);
        $this->db->delete('peliculas');
    }

}